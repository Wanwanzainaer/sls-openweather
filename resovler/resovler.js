const axios = require("../services/axiosConfig");
const openApiKey = require("../config/key");

function queryTranslateToAPIParams(query) {
  const { zip, cityName, countryCode } = query;
  if (zip)
    return countryCode ? { zip: `${zip},${countryCode}` } : { zip };
  if (cityName)
    return countryCode
      ? { q: `${cityName},${countryCode}` }
      : { q: cityName };

  return query;
}

exports.fetchCurrentWeatherData = async function(query) {
  const url = "/weather";
  query = queryTranslateToAPIParams(query);
  try {
    const { data } = await axios.get(url, {
      params: { ...query, appId: openApiKey.openWeatherAPIKey }
    });
    return data;
  } catch (e) {
    return e;
  }
};
// the api must to pay for the request
exports.fetchHourlyForecastData = async function(query) {
  const url = "/forecast/hourly";
  query = queryTranslateToAPIParams(query);
  try {
    const { data } = await axios.get(url, {
      params: { ...query, appId: openApiKey.openWeatherAPIKey }
    });
    return data;
  } catch (e) {
    return e;
  }
};
exports.fetchFiveDayForecastData = async function(query) {
  const url = "/forecast";
  query = queryTranslateToAPIParams(query);
  try {
    const { data } = await axios.get(url, {
      params: { ...query, appId: openApiKey.openWeatherAPIKey }
    });
    return data;
  } catch (e) {
    return e;
  }
};
